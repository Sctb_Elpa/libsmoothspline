﻿/*
 * SmoothSpline.c
 *
 * Created: 21.02.2014 8:02:38
 *  Author: galaktionov
 */

#include <stdlib.h>

#include "SmoothSpline.h"

void SplineApprox(double *pX, double *pY, uint32_t NWin, double pg, spline_range **result)
{
    *result = (spline_range*)malloc((NWin - 1) * sizeof(spline_range));
    double x[NWin+1];
    double y[NWin+1];
    double A[NWin+1];
    double B[NWin+1];
    double C[NWin+1];
    double D[NWin+1];
    double E[NWin+1];
    double F[NWin+1];
    double P[NWin+1];
    double Q[NWin+1];
    double CM[NWin+1];
    double ym[NWin+1];

    double y1, yn;
    uint32_t k, i;
    uint32_t ii, i1, i2;
    uint32_t N1, N2, N3;
    double h1, h2;
    double hii, hi, hi1;
    double hn1, hn2;

    double CP, CB, CK;

    uint32_t k1, k2;
    double dx;

    //pg = 1/pg;//нормируем значение чтобы базово работать с целыми (0...1000)

    for (k=1; k<=NWin; k++)
    {
        x[k] = ((double)(*(pX+k-1)));
        y[k] = (double)(*(pY+k-1));
    }
    N1 = NWin-1;
    N2 = NWin-2;
    N3 = NWin-3;
    y1 = (y[2] - y[1])/(x[2] - x[1]);
    yn = (y[NWin]-y[N1])/(x[NWin]-x[N1]);
    h1 = (double) (x[2]-x[1]);
    h2 = (double) (x[3]-x[2]);
    hn1 = (double) (x[NWin]-x[N1]);
    hn2 = (double) (x[N1]-x[N2]);
    C[1] = h1/3 + 2/(h1*h1) * pg;
    D[1] = h1/6 - 1/h1 * (1/h1 + 1/h2) * pg - pg / (h1*h1);
    E[1] = pg/(h1 * h2);
    B[2] = D[1];
    A[3] = E[1];
    A[NWin] = pg/(hn1 * hn2);
    B[NWin] = hn1/6 - 1/hn1 * (1/hn1 + 1/hn2) * pg - pg/(hn1*hn1);
    C[NWin] = hn1/3 + 2/(hn1*hn1) * pg;
    D[N1] = B[NWin];
    E[N2] = A[NWin];
    F[1] = (y[2] - y[1])/h1 - y1;
    F[NWin] = yn - (y[NWin] - y[N1])/hn1;

    for (i=2; i<=N2; i++)
    {
        ii = i - 1;
        i1 = i + 1;
        i2 = i + 2;
        hii = (double) (x[i] - x[ii]);
        hi =(double) (x[i1] - x[i]);
        hi1 = (double) (x[i2] - x[i1]);
        D[i] = hi/6 - 1/hi * ((1/hii + 1/hi)*pg + (1/hi + 1/hi1)*pg);
        B[i1] = D[i];
    }
    for (i=2; i<=N3; i++)
    {
        i1 = i + 1;
        i2 = i + 2;
        hi =(double) (x[i1] - x[i]);
        hi1 = (double) (x[i2] - x[i1]);
        E[i] = pg/(hi * hi1);
        A[i2] = E[i];
    }
    for (i=2; i<=N1; i++)
    {
        ii = i - 1;
        i1 = i + 1;
        i2 = i + 2;
        hii = (double) (x[i] - x[ii]);
        hi =(double) (x[i1] - x[i]);
        F[i] = (y[i1] - y[i])/hi - (y[i] - y[ii])/hii;
        C[i] = (hii + hi)/3 + 1/(hii*hii)*pg + (1/hii + 1/hi)*(1/hii + 1/hi)*pg + pg/(hi*hi);
    }
    //решение системы линейных уравнений
    //с пятидиагональной матрицей
    P[1] = -D[1]/C[1];
    Q[1] = -E[1]/C[1];
    CM[1] = F[1]/C[1];
    CP = C[2] + B[2]*P[1];
    P[2] = -(D[2] + B[2]*Q[1])/CP;
    Q[2] = -E[2]/CP;
    CM[2] = (F[2] - B[2]*CM[1])/CP;
    E[N1] = 0;
    D[NWin] = 0;
    E[NWin] = 0;
    for (i=3; i<=NWin; i++)
    {
        i1 = i - 1;
        i2 = i - 2;
        CB = A[i]*P[i2] + B[i];
        CK = C[i] + CB*P[i1] + A[i]*Q[i2];
        P[i] = -(D[i] + CB*Q[i1])/CK;
        Q[i] = -E[i]/CK;
        CM[i] = (F[i] - CB*CM[i1] - A[i]*CM[i2])/CK;
    }
    CM[N1] = P[N1]*CM[NWin] + CM[N1];
    for (i=3; i<=NWin; i++)
    {
        k = NWin - i + 1;
        k1 = k + 1;
        k2 = k + 2;
        CM[k] = P[k]*CM[k1] + Q[k]*CM[k2] + CM[k];
    }
    //систему порешали

    for (i=2; i<=N1; i++)
    {
        ii = i - 1;
        i1 = i + 1;
        hii = x[i] - x[ii];
        hi = x[i1] - x[i];
        ym[i] = y[i] - pg*((CM[i1] - CM[i])/hi - (CM[i] - CM[ii])/hii);
    }
    ym[1] = y[1] - pg*(CM[2] - CM[1])/h1;
    ym[NWin] = y[NWin] + pg*(CM[NWin] - CM[N1])/hn1;

    (*result)[0].Xmin = x[1];

    for (i=2; i<=NWin; i++)
    {
        uint32_t res_index = i - 2;

        hi = x[i] - x[i-1];

        double _a = (CM[i] - CM[i-1])/(6*hi);//A0[i]
        double _b = CM[i-1]/2;//A1[i]
        double _c = (ym[i] - ym[i-1])/hi - (2*CM[i-1] + CM[i])*hi/6;//A2[i]
        double _d = ym[i-1];//A3[i]

        (*result)[res_index].Xmax = x[i];
        if (res_index > 0)
            (*result)[res_index].Xmin = (*result)[res_index - 1].Xmax;
        (*result)[res_index].A = _a;
        (*result)[res_index].B = _b;
        (*result)[res_index].C = _c;
        (*result)[res_index].D = _d;
    }
}

calc_result calculateSplineAt(spline_range spline[], int size, double x)
{
    calc_result res = {0, 0, 0};
    int i;
    for (i = 0; i < size; ++i)
    {
        if ((x >= spline[i].Xmin) && (x <= spline[i].Xmax))
        {
            x -= spline[i].Xmin;
            double x2 = x*x;
            double x3 = x2*x;
            res.Y = spline[i].A * x3 + spline[i].B * x2 + spline[i].C * x + spline[i].D;
            res.d1Y = 3 * spline[i].A * x2 + 2 * spline[i].B * x + spline[i].C;
            res.d2Y = 6 * spline[i].A * x + 2 * spline[i].B;
            break;
        }
    }
    return res;
}

