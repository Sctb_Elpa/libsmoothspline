package ru.sctbelpa.libsmoothspline;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertTrue;

public class SplineTests {
    static List<Point> genRandomData() {
        return genRandomData(50);
    }

    static List<Point>  genData(UnaryOperator<Double> f, int count) {
        List<Point> res = new ArrayList<>(count);

        double step = 1.0 / count;
        for (int pos = 0; pos < count; ++pos) {
            res.add(new Point(pos * step, f.apply((double)pos)));
        }
        return res;
    }

    static List<Point> genRandomData(int count) {
        List<Point> res = new ArrayList<>(count);

        final long start = System.currentTimeMillis();

        for(int i = 0; i < count; ++i) {
            Point p = new Point(start + (i * 1000), Math.random());
            res.add(p);
        }

        return res;
    }

    //----------------------------------------------------------------------------------------------

    @Test
    public void testCopy() {
        final SmoothSpline smoothSpline = new SmoothSpline(genRandomData());
        final SmoothSpline copy = new SmoothSpline(smoothSpline);
    }

    @Test
    public void testArgumentIncrease() {
        List<Point> data = genData(X -> { return 10 * Math.sin(X); }, 20);

        final SmoothSpline s = new SmoothSpline(data);

        ISplineFragment f_start = s.find_fragment(data.get(0).X);
        ISplineFragment f_end = s.find_fragment(data.get(data.size() - 1).X);

        assertTrue(f_start.Xmin() < f_start.Xmax());
        assertTrue(f_end.Xmin() < f_end.Xmax());
    }

    @Test
    public void testArgumentdecrease() {
        List<Point> data = genData(X -> { return 10 * Math.sin(X); }, 20);

        Collections.reverse(data);

        final SmoothSpline s = new SmoothSpline(data);

        ISplineFragment f_start = s.find_fragment(data.get(0).X);
        ISplineFragment f_end = s.find_fragment(data.get(data.size() - 1).X);

        assertTrue(f_start.Xmin() < f_start.Xmax());
        assertTrue(f_end.Xmin() < f_end.Xmax());
    }

    @Test
    public void testUpdate() {
        List<Point> data = genData(X -> { return 10 * Math.sin(X); }, 20);

        SmoothSpline s = new SmoothSpline();
        s.Update(data);

        ISplineFragment f_start = s.find_fragment(data.get(0).X);
        ISplineFragment f_end = s.find_fragment(data.get(data.size() - 1).X);

        assertTrue(f_start.Xmin() < f_start.Xmax());
        assertTrue(f_end.Xmin() < f_end.Xmax());
    }

    @Test
    public void testEmpty() {
        SmoothSpline s = new SmoothSpline();

        assertTrue(Double.isNaN(s.Y(0)));
        assertTrue(Double.isNaN(s.dY(0)));
        assertTrue(Double.isNaN(s.d2Y(0)));
    }

    // see result in excell
    @Test
    public void testRealData() {
        int count = 20;
        AtomicInteger i = new AtomicInteger();

        List<Point> data = Stream.generate(
                () -> {
                    double x = Math.PI / count * i.getAndIncrement();
                    double y = Math.exp(-x) * Math.sin(x * x + Math.PI / 4) + Math.cos(x * 5) / 6.0;
                    return new Point(x, y);
                }).limit(count).collect(Collectors.toList());

        SmoothSpline s = new SmoothSpline(data, 0.01);

        for (Point p: data) {
            System.out.printf("%f;%f;%f\n", p.X, p.Y, s.Y(p.X));
        }
    }
}