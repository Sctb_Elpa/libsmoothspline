package ru.sctbelpa.libsmoothspline;

import java.util.List;

class ListExtantions {
    public interface Function<T, U> {
        U apply(T arg);
    }

    static <T> T binary_search(List<T> list, final Function<T, Integer> Criteria) {

        if (list == null || list.isEmpty())
            return null;

        int first = 0;
        int last = list.size();

        final int _end = last;

        while (first < last) {
            final int middle = first + (last - first) / 2;
            final T midle_val = list.get(middle);
            final int comp_res = Criteria.apply(midle_val);

            if (comp_res == 0) {
                return midle_val;
            } else if (comp_res > 0) {
                first = middle + 1;
            } else {
                last = middle - 1;
            }
        }

        if (last >= _end || last < 0) {
            return null;
        }
        final T last_val = list.get(last);

        return Criteria.apply(last_val) == 0 ? last_val : null;
    }
}
