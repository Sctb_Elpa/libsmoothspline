package ru.sctbelpa.libsmoothspline;

public class SplineFragment implements ISplineFragment {

    private double A, B, C, D, m_Xmin, m_Xmax;

    public SplineFragment(final double A, final double B, final double C, final double D,
                          final double Xmin, final double Xmax) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;

        m_Xmin = Xmin;
        m_Xmax = Xmax;
    }

    public SplineFragment(final double A, final double B, final double C, final double D) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;

        m_Xmin = Double.MIN_VALUE;
        m_Xmax = Double.MAX_VALUE;
    }

    @Override
    public double Y(double x) {
        x = x - m_Xmin;
        double x2 = x * x;
        return A * x2 * x + B * x2 + C * x + D;
    }

    @Override
    public double dY(double x) {
        x = x - m_Xmin;
        return 3 * A * x * x + 2 * B * x + C;
    }

    @Override
    public double d2Y(double x) {
        x = x - m_Xmin;
        return 6 * A * x + 2 * B;
    }

    @Override
    public double Xmin() {
        return m_Xmin;
    }

    @Override
    public double Xmax() {
        return m_Xmax;
    }

    @Override
    public PointInFragment isXin(double x) {
        if (x < m_Xmin) {
            return PointInFragment.LESS;
        } else if (x > m_Xmax) {
            return PointInFragment.OVER;
        } else {
            return PointInFragment.IN;
        }
    }

    @Override
    public ISplineFragment clone() {
        return new SplineFragment(A, B, C, D, m_Xmin, m_Xmax);
    }

    @Override
    public double getZero_d2Y() {
        // 6 * A * x + 2 * B == 0
        // где x = X - m_Xmin

        double zero_d2Y_x =  -B / (3.0 * A);
        double range = m_Xmax - m_Xmin;
        return (zero_d2Y_x > 0) && (zero_d2Y_x < range)
                ? zero_d2Y_x + m_Xmin
                : Double.NaN;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder
                .append("SplineFragment: { ")
                .append("Xmin: ").append(m_Xmin)
                .append(", Xmax: ").append(m_Xmax)
                .append(", A: ").append(A)
                .append(", B: ").append(B)
                .append(", C: ").append(C)
                .append(", D: ").append(D)
                .append(" }");

        return builder.toString();
    }
}
