package ru.sctbelpa.libsmoothspline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SmoothSpline {

    private List<ISplineFragment> spline_fragments;
    private ISplineFragment last_fragment = null; // cache last fragment
    private final EmptySplineFragment emptyFragment = new EmptySplineFragment();
    private boolean x_increasing;

    //----------------------------------------------------------------------------------------------

    private class Calc_object {
        final int NWin;
        final double pg;

        double[][] arrays;
        double[] A, B, C, D, E, F, P, Q, CM, ym;

        int lastN, N1, N2, N3;

        double y1, h1, h2;
        double yn, hn1, hn2;

        public  Calc_object(int NWin, double pg) {
            lastN = NWin - 1;
            N1 = NWin - 2;
            N2 = NWin - 3;
            N3 = NWin - 4;

            this.NWin = NWin;
            this.pg = pg;

            arrays = new double[10][NWin];

            A = arrays[0];
            B = arrays[1];
            C = arrays[2];
            D = arrays[3];
            E = arrays[4];
            F = arrays[5];
            P = arrays[6];
            Q = arrays[7];
            CM = arrays[8];
            ym = arrays[9];
        }

        void prepare(ListIterator<Point> Points_start, ListIterator<Point> Points_end) {
            final Point it_P0 = Points_start.next();
            final Point it_P1 = Points_start.next();
            final Point it_P2 = Points_start.next();

            final Point it_PlastN = Points_end.previous();
            final Point it_PN1 = Points_end.previous();
            final Point it_PN2 = Points_end.previous();

            y1 = (it_P1.Y - it_P0.Y) / (it_P1.X - it_P0.X);
            h1 = it_P1.X - it_P0.X;
            h2 = it_P2.X - it_P1.X;

            C[0] = h1 / 3.0 + 2.0 / (h1 * h1) * pg;
            D[0] = h1 / 6.0 - 1.0 / h1 * (1.0 / h1 + 1.0 / h2) * pg - pg / (h1 * h1);
            E[0] = pg / (h1 * h2);
            F[0] = (it_P1.Y - it_P0.Y) / h1 - y1;
            B[1] = D[0];
            A[2] = E[0];

            yn = (it_PlastN.Y - it_PN1.Y) / (it_PlastN.X - it_PN1.X);
            hn1 = it_PlastN.X - it_PN1.X;
            hn2 = it_PN1.X - it_PN2.X;

            A[lastN] = pg / (hn1 * hn2);
            B[lastN] = hn1 / 6.0 - 1.0 / hn1 * (1.0 / hn1 + 1.0 / hn2) * pg - pg / (hn1 * hn1);
            C[lastN] = hn1 / 3.0 + 2.0 / (hn1 * hn1) * pg;
            D[N1] = B[lastN];
            E[N2] = A[lastN];
            F[lastN] = yn - (it_PlastN.Y - it_PN1.Y) / hn1;
        }

        public void Calc_st1(List<Point> points) {
            Iterator<Point> P_ii = points.iterator();
            Iterator<Point> P_i = points.iterator();
            Iterator<Point> P_i1 = points.iterator();
            Iterator<Point> P_i2 = points.iterator();

            P_i.next();

            P_i1.next();
            P_i1.next();

            P_i2.next();
            P_i2.next();
            P_i2.next();

            for (int i = 1; i <= N1; ++i) {
                final Point P_ii_v = P_ii.next();
                final Point P_i_v = P_i.next();
                final Point P_i1_v = P_i1.next();

                final int i1 = i + 1;

                final double hii = P_i_v.X - P_ii_v.X;
                final double hi = P_i1_v.X - P_i_v.X;

                F[i] = (P_i1_v.Y - P_i_v.Y) / hi - (P_i_v.Y - P_ii_v.Y) / hii;

                C[i] = (hii + hi) / 3 + 1 / (hii * hii) * pg +
                        (1 / hii + 1 / hi) * (1 / hii + 1 / hi) * pg + pg / (hi * hi);

                if (i < N2 + 1) {
                    final int i2 = i + 2;
                    final Point P_i2_v = P_i2.next();

                    final double hi1 = P_i2_v.X - P_i1_v.X;

                    D[i] = hi / 6.0 - 1.0 / hi *
                            ((1.0 / hii + 1.0 / hi) * pg +
                                    (1.0 / hi + 1.0 / hi1) * pg);

                    B[i1] = D[i];
                    if (i < N3 + 1) {
                        E[i] = pg / (hi * hi1);
                        A[i2] = E[i];
                    }
                }
            }
        }

        void solve(List<Point> points) {
            P[1] = -D[1] / C[1];
            Q[1] = -E[1] / C[1];
            CM[1] = F[1] / C[1];

            final double CP = C[2] + B[2] * P[1];
            P[2] = -(D[2] + B[2] * Q[1]) / CP;
            Q[2] = -E[2] / CP;
            CM[2] = (F[2] - B[2] * CM[1]) / CP;
            E[N1] = 0;
            D[lastN] = 0;
            E[lastN] = 0;

            for (int i = 2; i < lastN; ++i) {
                final int i1 = i - 1;
                final int i2 = i - 2;
                final double CB = A[i] * P[i2] + B[i];
                final double CK = C[i] + CB * P[i1] + A[i] * Q[i2];
                P[i] = -(D[i] + CB * Q[i1]) / CK;
                Q[i] = -E[i] / CK;
                CM[i] = (F[i] - CB * CM[i1] - A[i] * CM[i2]) / CK;
            }

            CM[N1] = P[N1] * CM[lastN] + CM[N1];

            for (int i = 2; i < lastN; ++i) {
                final int k = lastN - i;
                final int k1 = k + 1;
                final int k2 = k + 2;
                CM[k] = P[k] * CM[k1] + Q[k] * CM[k2] + CM[k];
            }

            {
                Iterator<Point> P_ii = points.iterator();
                Iterator<Point> P_i = points.iterator();
                Iterator<Point> P_i1 = points.iterator();

                P_i.next();

                P_i1.next();
                P_i1.next();

                for (int i = 1; i <= N1; ++i) {
                    final Point P_ii_v = P_ii.next();
                    final Point P_i_v = P_i.next();
                    final Point P_i1_v = P_i1.next();

                    final int ii = i - 1;
                    final int i1 = i + 1;

                    final double hii = P_i_v.X - P_ii_v.X;
                    final double hi = P_i1_v.X - P_i_v.X;
                    ym[i] = P_i_v.Y - pg * ((CM[i1] - CM[i]) / hi - (CM[i] - CM[ii]) / hii);
                }
            }

            {
                final Iterator<Point> P_i = points.iterator();
                final Point P0 = P_i.next();
                final Point P1 = P_i.next();

                final ListIterator<Point> revItherator = points.listIterator(points.size());
                final Point P_lastN = revItherator.previous();
                final Point P_N1 = revItherator.previous();

                ym[0] = P0.Y - pg * (CM[1] - CM[0]) / (P1.X - P0.X);
                ym[lastN] = P_lastN.Y + pg * (CM[lastN] - CM[N1]) / (P_lastN.X - P_N1.X);
            }
        }
    }

    void clear_fragments() {
        spline_fragments = null;
    }

    public void createFragments(List<Point> points, final Calc_object data) {
            clear_fragments();
            spline_fragments = new ArrayList<>(data.NWin - 1);

            final Iterator<Point> begin_i = points.iterator(); // nextof begin_im1
            final Iterator<Point> begin_im1 = points.iterator();

            {
                Point last = points.get(points.size() - 1);
                Point begin = begin_i.next(); // nextof begin_im1
                x_increasing = begin.X <= last.X;
            }

            for (int i = 1; i <= data.lastN; ++i) {
                final Point begin_i_v = begin_i.next();
                final Point begin_im1_v = begin_im1.next();

                final int res_index = i - 1;
                final double point_i_X = begin_i_v.X;
                final double point_res_index_X = begin_im1_v.X;
                final double hi = point_i_X - point_res_index_X;

                double min = point_i_X;
                if (x_increasing) {
                    min = (res_index > 0) 
                            ? spline_fragments.get(res_index - 1).Xmax()
                            : points.get(0).X;
                }
                double max = x_increasing ? point_i_X : point_res_index_X;

                spline_fragments.add(new SplineFragment(
                        (data.CM[i] - data.CM[res_index]) / (6 * hi),
                        data.CM[res_index] / 2,
                        (data.ym[i] - data.ym[res_index]) / hi -
                                (2 * data.CM[res_index] + data.CM[i]) * hi / 6,
                        data.ym[res_index],
                        min, max));
            }
    }

    //----------------------------------------------------------------------------------------------

    public SmoothSpline() { }

    public SmoothSpline(final List<Point> points) {
        Update(points, 0);
    }

    public SmoothSpline(final List<Point> points, final double pg) {
        Update(points, pg);
    }

    SmoothSpline(final SmoothSpline lh) {
        spline_fragments = new ArrayList<>(lh.spline_fragments);
        x_increasing = lh.x_increasing;

        if (lh.last_fragment != null) {
            // copy pointer against new vector
            final int lh_offset = lh.spline_fragments.indexOf(lh.last_fragment);
            last_fragment = spline_fragments.get(lh_offset);
        }
    }

    public void Update(final List<Point> points) {
        Update(points, 0);
    }

    public void Update(final List<Point> points, final double pg) {
        final int Nwin = points.size();
        if (Nwin == 0) {
            spline_fragments = null;
            return;
        }

        final Calc_object data = new Calc_object(Nwin, pg);

        data.prepare(points.listIterator(), points.listIterator(points.size()));
        data.Calc_st1(points);
        data.solve(points);

        createFragments(points, data);
    }

    public ISplineFragment find_fragment(final double x) {
        if (last_fragment != null && last_fragment.isXin(x) == PointInFragment.IN)
            return last_fragment;

        final boolean x_increasing = this.x_increasing;

        final ISplineFragment res = ListExtantions.binary_search(spline_fragments,
                fragment -> {
                    final PointInFragment in = fragment.isXin(x);
                    switch (in) {
                        case IN:
                            return 0;
                        case LESS:
                            return x_increasing ? -1 : 1;
                        case OVER:
                            return x_increasing ? 1 : -1;
                        default:
                            return -1;
                    }
                });

        if (res != null) {
            last_fragment = res;
            return res;
        } else {
            // not found
            last_fragment = null;
            return emptyFragment;
        }
    }

    public double Y(final double X) { return find_fragment(X).Y(X); }
    public double dY(final double X) { return find_fragment(X).dY(X); }
    public double d2Y(final double X) { return find_fragment(X).d2Y(X); }
}
