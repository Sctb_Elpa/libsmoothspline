package ru.sctbelpa.libsmoothspline;

public class EmptySplineFragment implements ISplineFragment {

    @Override
    public double Y(double x) {
        return Double.NaN;
    }

    @Override
    public double dY(double x) {
        return Double.NaN;
    }

    @Override
    public double d2Y(double x) {
        return Double.NaN;
    }

    @Override
    public double Xmin() {
        return Double.NaN;
    }

    @Override
    public double Xmax() {
        return Double.NaN;
    }

    @Override
    public PointInFragment isXin(double x) {
        return PointInFragment.IN;
    }

    @Override
    public ISplineFragment clone() {
        return new EmptySplineFragment();
    }

    @Override
    public double getZero_d2Y() {
        return Double.NaN;
    }

    @Override
    public String toString() {
        return "EmptySplineFragment: { }";
    }
}
