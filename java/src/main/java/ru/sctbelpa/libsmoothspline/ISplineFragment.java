package ru.sctbelpa.libsmoothspline;

public interface ISplineFragment extends Cloneable {
    double Y(double x);
    double dY(double x);
    double d2Y(double x);
    double Xmin();
    double Xmax();
    PointInFragment isXin(double x);
    ISplineFragment clone();

    double getZero_d2Y();
}
