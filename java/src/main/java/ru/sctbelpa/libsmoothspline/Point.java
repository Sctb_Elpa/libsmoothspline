package ru.sctbelpa.libsmoothspline;

public class Point implements Cloneable {
    public double X, Y;

    public Point() {
        X = 0;
        Y = 0;
    }

    public Point(double x, double y) {
        X = x;
        Y = y;
    }

    public Point(final Point p) {
        X = p.X;
        Y = p.Y;
    }

    @Override
    public Point clone() {
        return new Point(X, Y);
    }
}
