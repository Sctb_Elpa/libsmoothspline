package ru.sctbelpa.libsmoothspline;

public enum PointInFragment {
    LESS,
    IN,
    OVER,
}
