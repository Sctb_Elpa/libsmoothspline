Библиотека построения сглаживающего сплайна через точки экспирементальных данных

Требуется:
    * >=git-1.7 (Получение исходных кодов)
	
Сборка:
    * Скачать исхохный код при помощи git
    $ git clone git clone https://bitbucket.org/Olololshka/libmylog/libsmoothspline

    * Создаем каталог для сборки
    $ mkdir libsmoothspline/build && cd libsmoothspline/build

    * запуск cmake
    ** для windows:
            $ cmake .. -G"MSYS Makefiles"
    ** для Linux:
            $ cmake ..

    * Сборка
    make

    * Установка (для Linux требуются права администратора).
      Каталог установки по-умолчанию /usr/local, что соответствует
      <каталог установки MinGW>/msys/1.0/local в Windows
    # make install

    * Сборка документации
    $ make doc

Пример использования:

#define SMOOTH_COEFF	(200) // коэфициент сглаживания
#define STEP			(0.1) // щаг между точками для прорисовки

...

// исходные данные
double Xdata[] = {...};
double Ydata[] = {...};

spline_range *spline; // сюда будет сохранена таблица коэфициентов сплайна
SplineApprox(Xdata, Ydata, sizeof(Xdata) / sizeof(double), SMOOTH_COEFF, &spline); // вычисление коэфициентов

...

double X = XStart; // начальное значение X, чтобы построить сплайн MIN(Xdata) <= XStart <= MAX(Xdata)
while(X <= XEnd) // MIN(Xdata) <= XEnd <= MAX(Xdata)
{
	calc_result result = calculateSplineAt(spline, sizeof(Xdata) / sizeof(double) - 1, X); // считаем
	printf("X = %f, Y = %f, d1y = %f, d2y = %f", X, result.Y, result.d1Y, result.d2Y); // вывод
    X += STEP;
}

### Примесание:

Избегайте больших разниц приращений по X и Y у исходных данных, например deltaX ~ 1000, delyaY ~ 0.1