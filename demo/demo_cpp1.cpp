#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <regex>
#include <math.h>
#include <list>

#include "SmoothSpline++.h"

using namespace smsp;

// 14.07.2017 09:20:57.538
static const std::regex re("(\\d+)\\.(\\d+)\\.(\\d+) (\\d+):(\\d+):(\\d+)\\.(\\d+)");

double date2seconds(const std::string &date, bool* isok) {
    std::smatch match;
    if(std::regex_search(date, match, re)) {
        struct tm t = {0};

        t.tm_mday = atoi(match[1].str().c_str());
        t.tm_mon = atoi(match[2].str().c_str());
        t.tm_year = atoi(match[3].str().c_str()) - 1900;

        t.tm_hour = atoi(match[4].str().c_str());
        t.tm_min = atoi(match[5].str().c_str());
        t.tm_sec = atoi(match[6].str().c_str());

        int msec = atoi(match[7].str().c_str());

        *isok = true;

        return (double)mktime(&t) + msec / 1000.0;
    }
    *isok = false;
    return 0;
}


int main(int argc, char* argv[]) {
    if (argc == 1) {
        printf("Usage: %s <datafile> [smoothfactor > 0]\n", argv[0]);
        return 1;
    }
    double smoothfactor = 0.0;

    if (argc >= 3) {
        smoothfactor = atof(argv[2]);
    }

    std::ifstream infile;
    infile.open(argv[1]);

    std::list<double> t;
    std::list<double> values;

    std::vector<Point<double>> data;

    double prev_ts = NAN;
    for( std::string line; getline( infile, line ); )
    {
        int det = line.find(';');
        bool dateok;
        double date = date2seconds(line.substr(0, det), &dateok);
        if (!dateok) continue;

        // unic T
        if (date == prev_ts) continue;
        prev_ts = date;

        std::string val_str = line.substr(det + 1);
        std::replace(val_str.begin(), val_str.end(), ',', '.');
        val_str = val_str.substr(0, val_str.find(';'));

        double value = atof(val_str.c_str());

        data.emplace_back(Point<double>{date, value});
    }

    SmoothSpline<double> spline(data, smoothfactor);

    std::for_each(data.cbegin(), data.cend(), [&spline](const Point<double>& p) {
         const auto &fragment = spline.find_fragment(p.X);
         const auto X = p.X;
         printf("%f;%f;%f;%f;%f\n", X, p.Y, fragment.Y(X), fragment.dY(X), fragment.d2Y(X));
    });

    return 0;
}

